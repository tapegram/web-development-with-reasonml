// type shirtSize = string;
// let mySize = "Medium";
// let otherSize = "Large";
// let wrongSize = "M";

type shirtSize =
  | Small
  | Medium
  | Large
  | XLarge(int);

let price = (size: shirtSize): float => {
  switch (size) {
  | Small => 11.00
  | Medium => 12.50
  | Large => 14.00
  | XLarge(n) => 16.00 +. float_of_int(n - 1) *. 0.50
  };
};

let toString = (size: shirtSize): string => {
  switch (size) {
  | Small => "S"
  | Medium => "M"
  | Large => "L"
  | XLarge(n) => String.make(n, 'X') ++ "L"
  };
};

let fromString = (size: string): option(shirtSize) => {
  switch (size) {
  | "S" => Some(Small)
  | "M" => Some(Medium)
  | "L" => Some(Large)
  | "XL" => Some(XLarge(1))
  | "XXL" => Some(XLarge(2))
  | "XXXL" => Some(XLarge(3))
  | _ => None
  };
};

let toFixed = Js.Float.toFixedWithPrecision;

// let displayPriceBad = (input: string): unit => {
//   let size = fromString(input);
//   let amount = price(size);
//   let text =
//     switch (amount) {
//     | Some(cost) =>
//       let costStr = toFixed(cost, ~digits=2);
//       {j|Your $input shirt costs \$$costStr.|j};
//     | None => {j|Can not determine price for $input|j}
//     };
//   Js.log(text);
// };

let makeDisplayText = (cost: option(float)): string => {
  switch (cost) {
  | Some(cost) =>
    let costStr = toFixed(cost, ~digits=2);
    {j|Your $input shirt costs \$$costStr.|j};
  | None => {j|Can not determine price for $input|j}
  };
};

let displayPrice = (input: string): unit => {
  fromString(input)->Belt.Option.map(_, price)->makeDisplayText->Js.log;
};

displayPrice("XXL");
