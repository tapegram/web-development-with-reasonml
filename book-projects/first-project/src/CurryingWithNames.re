open Functions;

let usPayments = payment(~years=30);
let ukPayments = payment(~years=25);
let dePayments = payment(~years=20);

Js.log("Loan of 10000 at 5%");
Js.log2(
  {js|US: $|js},
  toFixed(usPayments(~principal=10000.0, ~apr=5.0, ()), ~digits=2),
);
Js.log2(
  {js|US: $|js},
  toFixed(ukPayments(~principal=10000.0, ~apr=5.0, ()), ~digits=2),
);
Js.log2(
  {js|US: $|js},
  toFixed(dePayments(~principal=10000.0, ~apr=5.0, ()), ~digits=2),
);
