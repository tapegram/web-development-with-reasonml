let payment =
    (~principal as p: float, ~apr: float, ~years: int=30, (): unit): float => {
  let r = apr /. 12.0 /. 100.0;
  let n = float_of_int(years * 12);
  let powerTerm = (1.0 +. r) ** n;
  p *. (r *. powerTerm) /. (powerTerm -. 1.0);
};

let avg: (float, float) => float =
  (a, b) => {
    (a +. b) /. 2.;
  };

let principal = 10000.0;
let apr = 5.0;
let amount = payment(~principal, ~apr, ());

let sqr = x => x * x;
let toFixed = Js.Float.toFixedWithPrecision;

Js.log2("Amount per month: $", toFixed(amount, ~digits=2));
