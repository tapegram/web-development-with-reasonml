let windChill: (float, float) => float =
  (temperature, windVelocity) => {
    13.12
    +. 0.6215
    *. temperature
    -. 11.37
    *. windVelocity
    ** 0.16
    +. 0.3965
    *. temperature
    *. windVelocity
    ** 0.16;
  };
